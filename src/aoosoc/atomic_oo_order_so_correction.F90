!dirac_copyright_start
!      Copyright (c) by the authors of DIRAC.
!
!      This program is free software; you can redistribute it and/or
!      modify it under the terms of the GNU Lesser General Public
!      License version 2.1 as published by the Free Software Foundation.
!
!      This program is distributed in the hope that it will be useful,
!      but WITHOUT ANY WARRANTY; without even the implied warranty of
!      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!      Lesser General Public License for more details.
!
!      If a copy of the GNU LGPL v2.1 was not distributed with this
!      code, you can obtain one at https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html.
!dirac_copyright_end
!
!
! this module contains all the functionality required to add one- and two-electron
! spin-orbit corrections to the hamiltonian in DIRAC-sorted SA-AO basis.
! the spin-orbit corrections will be obtained as the spin-dependent parts of the
! (converged) 4c-Fock operator in an atomic Dirac run.
!
! written by sknecht august 2012
!
module atomic_oo_order_so_correction

  use atomic_oo_order_so_correction_cfg
  use atomic_oo_order_so_correction_utils
  use module_aoosoc_pfg
  use x2c_utils, only: print_x2cmat

  implicit none

  public get_atomic_oo_order_so_correction
  public put_atomic_oo_order_so_correction

  private

contains

!----------------------------------------------------------------------
  subroutine get_atomic_oo_order_so_correction(            &
                                               dmat,       &
                                               pct_mat,    &
                                               calc_2esoc, &
                                               lwork_ext,  &
                                               print_lvl   &
                                               )
!**********************************************************************
     real(8), intent(inout)             :: dmat(nrows,ncols,nr_quat,nr_2e_fock_matrices)
     real(8), intent(in)                :: pct_mat(*)
     logical, intent(in)                :: calc_2esoc
     integer, intent(in)                :: lwork_ext
     integer, intent(in)                :: print_lvl
!----------------------------------------------------------------------
     integer                            :: i
     real(8), allocatable               :: fmat(:)
!----------------------------------------------------------------------
      call hello_aoosoc()

!     if(nr_2e_fock_matrices > 1)call quit('aoosoc module does not work yet for atomic AOC-HF; use .FOCC in *SCF')

      allocate(fmat(nrows*ncols*nr_quat*nr_2e_fock_matrices))
      fmat = 0.0d0

!     get full (scalar+spin-orbit) 2e-fock matrix
      if(calc_2esoc)then
#define try_this
#if defined try_this
        call get_scso_2e_fock_aoc(                       &
                                  dmat,                  &
                                  fmat,                  &
                                  nrows,                 &
                                  ncols,                 &
                                  1,                     &
                                  nr_quat                &
                                 )
#else
        call get_scso_2e_fock(                           &
                              dmat,                      &
                              fmat,                      &
                              nrows,                     &
                              ncols,                     &
                              nr_2e_fock_matrices,       &
                              aoo_dfopen,                &
                              aoo_intflg,                &
                              aoo_mdirac,                &
                              nr_quat,                   &
                              aoo_cb_pq_to_uq(1,0),      &
                              aoo_ihqmat(1,-1),          &
                              lwork_ext,                 &
                              print_lvl                  &
                             )

#endif

!       debug print
        if(print_lvl > 2)then
          call print_x2cmat(                             &
                            fmat,                        &
                            nr_ao_total_aoo,             &
                            nr_ao_total_aoo,             &
                            nr_quat,                     &
                            aoo_cb_pq_to_uq(1,0),        &
                            'aoosoc - 2e-complete',      &
                            6                            &
                           )
        end if

!       normalize (picture-change transform) 2e-SO-fock matrix
        call get_normalized_2e_soc(                      &
                                   fmat,                 &
                                   pct_mat,              &
                                   dmat,                 &
                                   nr_ao_total_aoo,      &
                                   nr_ao_large_aoo,      &
                                   nr_ao_all,            &
                                   nr_ao_l,              &
                                   nr_fsym,              &
                                   nr_quat,              &
                                   aoo_cb_pq_to_uq,      &
                                   ioff_aomat_x,         &
                                   aoo_bs_to_fs,         &
                                   print_lvl             &
                                  )

!       debug print
!       if(print_lvl > 2)then
          call print_x2cmat(                             &
                            fmat,                        &
                            nr_ao_large_aoo,             &
                            nr_ao_large_aoo,             &
                            nr_quat,                     &
                            aoo_cb_pq_to_uq(1,0),        &
                            'aoosoc - 2e-normalized',    &
                            6                            &
                           )
!       end if
      end if

!     save spin-orbit parts on file (of atom(1) ... == atomic fragment)
      call dump_normalized_2e_soc(                            &
                                  fmat,                       &
                                  nr_ao_large_aoo,            &
                                  nr_ao_large_aoo,            &
                                  nr_quat,                    &
                                  file_name_aoo_contributions,&
                                  nint(atom(1)%charge),       &
                                  aoo_fsoc                    &
                                 )

      call goodbye_aoosoc()

  end subroutine get_atomic_oo_order_so_correction
!----------------------------------------------------------------------

  subroutine put_atomic_oo_order_so_correction(                             &
                                               Gmat,                        &
                                               naosh_l,                     &
                                               nrows_ext,                   &
                                               ncols_ext,                   &
                                               nr_quat_ext,                 &
                                               center_id,                   &
                                               center_total,                &
                                               max_center,                  &
                                               print_lvl                    &
                                              )
!**********************************************************************
     real(8), optional, intent(inout)   :: Gmat(*)
     integer, intent(in)                :: naosh_l
     integer, intent(in)                :: nrows_ext
     integer, intent(in)                :: ncols_ext
     integer, intent(in)                :: nr_quat_ext
     integer, intent(in)                :: center_id
     integer, intent(inout)             :: center_total
     integer, intent(in)                :: max_center
     integer, intent(in)                :: print_lvl
!----------------------------------------------------------------------
     real(8), allocatable               :: Gmat_frag(:)
     real(8), allocatable               :: rbuf(:)
     real(8), allocatable               :: cbuf(:)
     real(8), allocatable               :: ibuf(:)
     real(8), allocatable               :: jbuf(:)
!----------------------------------------------------------------------


      if(nint(atom(center_total)%charge) > 1)then

        allocate(Gmat_frag(nrows_ext*ncols_ext*4))
!       print *, 'charge, nrows_ext,total,id',nint(atom(center_total)%charge),nrows_ext,center_total,center_id
!       get spin-orbit parts from file (of atom(i) ... == atomic fragment)
        call read_normalized_2e_soc(                                 &
                                    Gmat_frag,                       &
                                    nrows_ext,                       &
                                    ncols_ext,                       &
                                    nr_quat_ext,                     &
                                    file_name_aoo_contributions,     &
                                    nint(atom(center_total)%charge), &
                                    aoo_fsoc                         &
                                   )
        allocate(rbuf(naosh_l))
        allocate(cbuf(naosh_l))
        allocate(ibuf(nrows_ext))
        allocate(jbuf(ncols_ext))

        rbuf = 0
        cbuf = 0
        ibuf = 0
        jbuf = 0

        call put_aoblock(Gmat,                            &
                         naosh_l,                         &
                         naosh_l,                         &
                         center_total,                    &
                         aoo_nont(center_id),             &
                         aoo_nucdeg(center_total),        &
                         4,                               &
                         Gmat_frag,                       &
                         nrows_ext,                       &
                         nrows_ext,                       &
                         -1,                              &
                         ncols_ext,                       &
                         ncols_ext,                       &
                         -1,                              &
                         rbuf,                            &
                         cbuf,                            &
                         ibuf,                            &
                         jbuf)

        deallocate(jbuf)
        deallocate(ibuf)
        deallocate(cbuf)
        deallocate(rbuf)
        deallocate(Gmat_frag)

      end if

!     update counter
      center_total = center_total + aoo_nont(center_id)
!     print *, 'center_total,  aoo_nont(center_id)',center_total,  aoo_nont(center_id)

  end subroutine put_atomic_oo_order_so_correction
!----------------------------------------------------------------------

  end module atomic_oo_order_so_correction
